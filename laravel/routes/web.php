<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('data-storage/{encryptedPath}', 'DataStorageController@index')->name('data-storage');

Route::middleware(['auth'])->group(function () {
    Route::view('/', 'dashboard')->name('dashboard');

    Route::prefix('product-categories')->name('product_categories.')->group(function () {
        Route::get('/', 'ProductCategoryController@index')->name('index');
        Route::get('/create', 'ProductCategoryController@create')->name('create');
        Route::post('/create', 'ProductCategoryController@store')->name('store');
        Route::get('/{id}', 'ProductCategoryController@edit')->name('edit');
        Route::post('/{id}', 'ProductCategoryController@update')->name('update');
        Route::delete('/{id}', 'ProductCategoryController@delete')->name('delete');
    });

    Route::prefix('products')->name('products.')->group(function () {
        Route::get('/', 'ProductController@index')->name('index');
        Route::get('/create', 'ProductController@create')->name('create');
        Route::post('/create', 'ProductController@store')->name('store');
        Route::get('/{id}', 'ProductController@edit')->name('edit');
        Route::post('/{id}', 'ProductController@update')->name('update');
        Route::delete('/{id}', 'ProductController@delete')->name('delete');
    });

    Route::prefix('expenses')->name('expenses.')->group(function () {
        Route::get('/', 'ExpenseController@index')->name('index');
        Route::get('/create', 'ExpenseController@create')->name('create');
        Route::post('/', 'ExpenseController@store')->name('store');
        Route::delete('/{id}', 'ExpenseController@delete')->name('delete');
    });

    Route::prefix('cashier')->name('cashier.')->group(function () {
        Route::get('/', 'CashierController@index')->name('index');
        Route::get('/print/{encryptedCode}', 'CashierController@print')->name('print');
    });

    Route::prefix('transactions')->name('transactions.')->group(function () {
        Route::prefix('manage')->name('manage.')->group(function () {
            Route::get('/', 'TransactionController@manage')->name('index');
            Route::get('{id}', 'TransactionController@show')->name('show');

            Route::get('{id}/edit', 'TransactionController@edit')->name('edit');
            Route::put('{id}', 'TransactionController@update')->name('update');
            Route::get('{id}/item/add', 'TransactionItemController@add')->name('items.add');
            Route::post('{id}/item', 'TransactionItemController@store')->name('items.store');
            Route::delete('{transactionId}/item/{itemId}', 'TransactionItemController@delete')->name('items.delete');

            Route::delete('{id}', 'TransactionController@delete')->name('delete');
        });


        Route::get('/most-sales-products', 'TransactionController@mostSalesProducts')->name('most-sales-products');
        Route::get('/profit-and-loss', 'TransactionController@profitAndLoss')->name('profit-and-loss');
    });

    Route::get('reports', 'ReportController@index')->name('reports.index');
    Route::get('reports/{code}', 'ReportController@detail')->name('reports.detail');

    Route::prefix('settings')->name('settings.')->group(function () {
        Route::get('account', 'SettingController@account')->name('account');
        Route::post('account', 'SettingController@updateAccount')->name('account.update');
        Route::get('shop', 'SettingController@shop')->name('shop');
        Route::post('shop', 'SettingController@updateShop')->name('shop.update');
    });
});
