<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Cashier extends Component
{
    use WithPagination;

    private $products;
    public $search;

    public $transactionCode;
    public $transactionDate;
    public $transactionItems = [];
    public $totalTransaction = 0;
    public $cash;
    public $change = 0;

    public $newItem;
    private $selectedProduct;

    public $printRoute = '#';

    /**
     * Mounting livewire component
     */
    public function mount()
    {
        $this->initNewItem();
    }

    /**
     * Render livewire component
     */
    public function render()
    {
        $products = Product::query()
            ->where('name', 'like', '%'.$this->search.'%');

        return view('livewire.cashier', [
            'products' => $products->where('stock', '>', 0)->orderBy('name')->paginate(10)
        ]);
    }

    /**
     * Initiate new item
     */
    public function initNewItem()
    {
        $this->newItem = [
            'product_id' => null,
            'product_name' => null,
            'product_category' => null,
            'product_image' => null,
            'product_size' => null,
            'product_weight' => null,
            'product_description' => null,
            'product_price' => null,
            'product_stock' => null,
            'quantity' => 0,
        ];

        $this->cash = null; $this->change = 0;
        $this->transactionDate = date('Y-m-d H:i:s');
    }

    /**
     * Add item
     */
    public function addItem($productId)
    {
        $this->selectedProduct = Product::query()->find($productId);
        if ($this->selectedProduct) {
            $this->newItem['product_id'] = $this->selectedProduct->id;
            $this->newItem['product_name'] = $this->selectedProduct->name;
            $this->newItem['product_category'] = $this->selectedProduct->category->name ?? null;
            $this->newItem['product_image'] = $this->selectedProduct->image;
            $this->newItem['product_size'] = $this->selectedProduct->size;
            $this->newItem['product_weight'] = $this->selectedProduct->weight;
            $this->newItem['product_description'] = $this->selectedProduct->description;
            $this->newItem['product_price'] = $this->selectedProduct->price;
            $this->newItem['product_stock'] = $this->selectedProduct->stock;
            $this->newItem['quantity'] = 1;
        }
    }

    public function addQuantity()
    {
        if ($this->newItem['quantity'] < $this->newItem['product_stock']) $this->newItem['quantity']++;
    }

    public function substractQuantity()
    {
        $this->newItem['quantity']--;
    }

    /**
     * Store item
     */
    public function storeItem()
    {
        $transactionItems = collect($this->transactionItems);
        $newItem = $this->newItem;

        if ($newItem['quantity'] > $newItem['product_stock']) return;

        $transactionItemProduct  = $transactionItems
            ->where('product_id', $newItem['product_id'])
            ->first();

        if ($transactionItemProduct) {
            $transactionItems = $transactionItems->map(function ($item) use ($newItem) {
                if ($item['product_id'] == $newItem['product_id']) {
                    $item['quantity'] += $newItem['quantity'];
                }

                return $item;
            });
        } else {
            $transactionItems->push($newItem);
        }

        $this->transactionItems = $transactionItems->toArray();
        $this->getTotalTransaction();
    }

    /**
     * remove Item
     */
    public function removeItem($productId)
    {
        $this->transactionItems = collect($this->transactionItems)
            ->filter(function ($item) use ($productId) {
                return $item['product_id'] != $productId;
            })->toArray();
        $this->getTotalTransaction();
    }

    /**
     * get total transaction
     */
    public function getTotalTransaction()
    {
        $transactions = collect($this->transactionItems)->transform(function ($item) {
            return [
                'total' => $item['product_price'] * $item['quantity']
            ];
        });

        $this->totalTransaction = $transactions->sum('total');
    }

    /**
     * count change
     */
    public function countChange()
    {
        $cash = (int) str_replace(',', '', $this->cash);
        $this->change = $cash - $this->totalTransaction;
    }

    /**
     * make payment
     */
    public function makePayment()
    {
        if (count($this->transactionItems) <= 0) return;
        $this->countChange();

        try {
            DB::beginTransaction();

            $transaction = new Transaction();
            $transaction->code = $transaction->getNewCode();
            $transaction->total_price = $this->totalTransaction;
            $transaction->cash = (int) str_replace(',', '', $this->cash);
            $transaction->change = $this->change;
            $transaction->save();

            foreach ($this->transactionItems as $item) {
                $product = Product::query()->find($item['product_id']);
                if ($product == null) break;
                if ($product->stock <= 0) break;
                if ($product->stock - $item['quantity'] < 0) break;

                $transactionItem = new TransactionItem();
                $transactionItem->product_id = $product->id;
                $transactionItem->qty = $item['quantity'];
                $transactionItem->total_price = $product->price * $transactionItem->qty;
                $transactionItem->transaction_id = $transaction->id;
                $transactionItem->save();

                $product->stock -= $transactionItem->qty;
                $product->save();
            }

            DB::commit();
            $this->transactionCode = $transaction->code;
            $this->transactionDate = $transaction->created_at->toString();
            $this->printRoute = route('cashier.print', encrypt($this->transactionCode));
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            dd($e);
        }
    }
}
