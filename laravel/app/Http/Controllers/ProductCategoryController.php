<?php

namespace App\Http\Controllers;

use App\DataTables\ProductCategoryDataTable;
use App\Models\ProductCategory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class ProductCategoryController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(ProductCategoryDataTable $dataTable)
    {
        return $dataTable->render('product-categories.index');
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('product-categories.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required'
        ]);

        try {
            $productCategory = new ProductCategory([
                'name' => $data['name']
            ]);
            $productCategory->save();

            return redirect(route('product_categories.index'))
                ->with([
                    'alert' => [
                        'message' => 'Berhasil menambahkan kategori produk baru.',
                        'class' => 'info'
                    ]
                ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput()
                ->withErrors($e);
        }
    }

    /**
     * Show edit page
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit(int $id)
    {
        return view('product-categories.edit', [
            'data' => ProductCategory::query()->findOrFail($id)
        ]);
    }

    /**
     * Update product category
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function update(int $id, Request $request)
    {
        $data = $request->validate([
            'name' => 'required'
        ]);

        try {
            $productCategory = ProductCategory::query()->findOrFail($id);
            $productCategory->name = $data['name'];
            $productCategory->save();

            return redirect(route('product_categories.index'))
                ->with([
                    'alert' => [
                        'message' => 'Berhasil memperbaharui kategori produk',
                        'class' => 'info'
                    ]
                ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Delete product category
     *
     * @param int $id
     * @return RedirectResponse|Redirector
     */
    public function delete(int $id)
    {
        try {
            ProductCategory::query()->findOrFail($id)->delete();

            return redirect(route('product_categories.index'))
                ->with([
                    'alert' => [
                        'message' => 'Berhasil menghapus kategori produk',
                        'class' => 'info'
                    ]
                ]);
        } catch (\Exception $e) {
            return redirect()->back()->withInput()
                ->withErrors($e->getMessage());
        }
    }
}