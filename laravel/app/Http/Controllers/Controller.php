<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Send alert flash message
     * 
     * @param $message
     * @return array
     */
    public function success($message)
    {
        return [
            'alert' => [
                'message' => $message,
                'class' => 'success'
            ]
        ];
    }

    /**
     * Send alert flash message
     *
     * @param $message
     * @return array
     */
    public function error($message)
    {
        return [
            'alert' => [
                'message' => $message,
                'class' => 'danger'
            ]
        ];
    }
}
