<?php

namespace App\Http\Controllers;

use App\DataTables\ProductDataTable;
use App\Models\Product;
use App\Models\ProductCategory;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(ProductDataTable $dataTable)
    {
        return $dataTable->render('products.index');
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('products.create', [
            'categories' => ProductCategory::query()->orderBy('name')->pluck('name', 'id')
        ]);
    }

    /**
     * Store product
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'price' => 'required|numeric|min:0',
            'size' => 'required',
            'weight' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpg,jpeg,png,bmp,tiff,gif',
            'stock' => 'min:0'
        ]);

        try {
            $product = new Product($request->except(['_token', 'image']));
            if ($request->hasFile('image')) {
                $product->image = $request->file('image')->store(Product::IMAGE_PATH);
            }
            $product->save();

            return redirect(route('products.index'))
                ->with([
                    'alert' => [
                        'message' => 'Berhasil menambahkan produk baru.',
                        'class' => 'info'
                    ]
                ]);
        } catch (Exception $e) {
            return redirect()->back()->withInput()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit(int $id)
    {
        return view('products.edit', [
            'categories' => ProductCategory::query()->orderBy('name')->pluck('name', 'id'),
            'data' => Product::query()->findOrFail($id)
        ]);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function update(int $id, Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'price' => 'required|numeric|min:0',
            'size' => 'required',
            'weight' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpg,jpeg,png,bmp,tiff,gif',
            'stock' => 'min:0'
        ]);

        try {
            $product = Product::query()->findOrFail($id);
            $product->fill($request->except('_token', 'image'));
            if ($request->hasFile('image')) {
                if ($product->image) Storage::delete($product->getAttributes('image'));
                $product->image = $request->file('image')->store(Product::IMAGE_PATH);
            }
            $product->save();

            return redirect(route('products.index'))
                ->with([
                    'alert' => [
                        'message' => 'Berhasil memperbaharui produk baru.',
                        'class' => 'info'
                    ]
                ]);
        } catch (Exception $e) {
            return redirect()->back()->withInput()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Delete product by Id
     *
     * @param $id
     * @return RedirectResponse|Redirector
     */
    public function delete($id)
    {
        try {
            $product = Product::query()->findOrFail($id);
            Storage::delete($product->getAttributes('image'));
            $product->delete();

            return redirect(route('products.index'))
                ->with([
                    'alert' => [
                        'message' => 'Berhasil menghapus produk baru.',
                        'class' => 'info'
                    ]
                ]);
        } catch (Exception $e) {
            return redirect()->back()->withInput()
                ->withErrors($e->getMessage());
        }
    }
}