<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function index()
    {
        $data = [];

        $data['current'] = [
            'sales' => Transaction::getSales(
                (new Carbon('now'))->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            ),
            'profit' => Transaction::getProfit(
                (new Carbon('now'))->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            )
        ];

        $data['daily'] = [
            'sales' => Transaction::getSales(
                (new Carbon('yesterday'))->format('Y-m-d'),
                (new Carbon('yesterday'))->format('Y-m-d')
            ),
            'profit' => Transaction::getProfit(
                (new Carbon('yesterday'))->format('Y-m-d'),
                (new Carbon('yesterday'))->format('Y-m-d')
            ),
            'net_profit' => Transaction::getNetProfit(
                (new Carbon('yesterday'))->format('Y-m-d'),
                (new Carbon('yesterday'))->format('Y-m-d')
            )
        ];

        $data['weekly'] = [
            'sales' => Transaction::getSales(
                (new Carbon('now'))->subDays(7)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            ),
            'profit' => Transaction::getProfit(
                (new Carbon('now'))->subDays(7)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            ),
            'net_profit' => Transaction::getNetProfit(
                (new Carbon('now'))->subDays(7)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            )
        ];

        $data['monthly'] = [
            'sales' => Transaction::getSales(
                (new Carbon('now'))->subDays(30)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            ),
            'profit' => Transaction::getProfit(
                (new Carbon('now'))->subDays(30)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            ),
            'net_profit' => Transaction::getNetProfit(
                (new Carbon('now'))->subDays(30)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            )
        ];

        $data['annual'] = [
            'sales' => Transaction::getSales(
                (new Carbon('now'))->subDays(365)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            ),
            'profit' => Transaction::getProfit(
                (new Carbon('now'))->subDays(365)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            ),
            'net_profit' => Transaction::getNetProfit(
                (new Carbon('now'))->subDays(365)->format('Y-m-d'),
                (new Carbon('now'))->format('Y-m-d')
            )
        ];

        return view('reports.index', $data);
    }

    public function detail(string $code)
    {
        switch ($code) {
            case 'daily':
                $dates = [
                    (new Carbon('yesterday'))->format('Y-m-d'),
                    (new Carbon('yesterday'))->format('Y-m-d')
                ];
                break;
            case 'weekly':
                $dates = [
                    (new Carbon('now'))->subDays(7)->format('Y-m-d'),
                    (new Carbon('now'))->format('Y-m-d')
                ];
                break;
            case 'monthly':
                $dates = [
                    (new Carbon('now'))->subDays(30)->format('Y-m-d'),
                    (new Carbon('now'))->format('Y-m-d')
                ];
                break;
            case 'annual':
                $dates = [
                    (new Carbon('now'))->subDays(365)->format('Y-m-d'),
                    (new Carbon('now'))->format('Y-m-d')
                ];
                break;
            default:
                $dates = [
                    'startDate' => (new Carbon('now'))->format('Y-m-d'),
                    'endDate' => (new Carbon('now'))->format('Y-m-d')
                ];
                break;
        }

        $data = Transaction::getData($dates[0], $dates[1]);

        return view('reports.detail', [
            'data' => $data,
            'dates' => $dates,
            'totalIncome' => Transaction::getProfit(...$dates),
            'totalProfit' => Transaction::getNetProfit(...$dates)
        ]);
    }
}