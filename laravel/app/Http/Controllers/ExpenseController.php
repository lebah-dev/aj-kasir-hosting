<?php

namespace App\Http\Controllers;

use App\DataTables\ExpenseDataTable;
use App\Models\Expense;
use App\Models\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ExpenseController extends Controller
{
    /**
     * @param ExpenseDataTable $dataTable
     * @return mixed
     */
    public function index(ExpenseDataTable $dataTable)
    {
        return $dataTable->render('expenses.index');
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        return view('expenses.create', [
            'products' => Product::query()
                ->select('products.*')
                ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                ->orderBy('product_categories.name')
                ->orderby('products.name')
                ->get(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'product_id' => 'required',
            'qty' => 'required|min:1',
            'price' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $product = Product::query()->find($data['product_id']);
            if ($product == null) return redirect()->back()
                ->withInput()
                ->with($this->error('Produk tidak ditemukan'));

            $expense = new Expense($data);
            $expense->qty = (int) $data['qty'];
            $expense->price = (double) str_replace(',', '', $data['price']);
            $expense->total_price = $expense->qty * $expense->price;
            $expense->save();

            $product->stock += $expense->qty;
            $product->save();

            DB::commit();
            return redirect()->route('expenses.index')
                ->with($this->success(__('alert.success.store')));
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return redirect()->back()
                ->withInput()
                ->with($this->error(__('alert.error.store')));
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();

            $expense = Expense::query()->find($id);
            $expense->product->stock -= $expense->qty;
            $expense->product->save();
            $expense->delete();

            DB::commit();
            return redirect()->route('expenses.index')
                ->with($this->success(__('alert.success.delete')));
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return redirect()->back()
                ->withInput()
                ->with($this->error(__('alert.error.delete')));
        }
    }
}