<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class SettingController extends Controller
{
    /**
     * @return Factory|View
     */
    public function account()
    {
        return view('settings.account', [
            'user' => auth()->user()
        ]);
    }

    public function updateAccount(Request $request)
    {
        $data = $request->validate([
            'avatar' => 'mimes:jpg,jpeg,png,gif,bmp',
            'name' => 'required',
            'email' => 'required|unique:users,email,' . auth()->id(),
            'phone_number' => 'required',
            'address' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $user = auth()->user();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->phone_number = $data['phone_number'];
            $user->address = $data['address'];

            if ($request->hasFile('avatar')) {
                Storage::delete($user->avatar);
                $user->avatar = $request->file('avatar')->store(User::AVATAR_PATH);
            }

            if ($request->password && $request->new_password) {
                if (password_verify($request->password, $user->password)) {
                    $user->password = bcrypt($request->new_password);
                } else {
                    return redirect()->back()->with($this->error('Password tidak sesuai'));
                }
            }

            $user->save();

            DB::commit();
            return redirect()->back()
                ->with($this->success(__('alert.success.update')));
        } catch (\Exception $e) {
            DB::commit();
            report($e);
            return redirect()->back()
                ->with($this->error(__('alert.error.update')));
        }
    }

    /**
     * @return Factory|View
     */
    public function shop()
    {
        return view('settings.shop', [
            'shop' => Shop::query()->where('is_active', true)->first()
        ]);
    }

    public function updateShop(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'slogan' => 'required',
            'description' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $shop = Shop::query()->where('is_active', true)
                ->first() ?: new Shop();
            $shop->name = $data['name'];
            $shop->email = $data['email'];
            $shop->phone_number = $data['phone_number'];
            $shop->address = $data['address'];
            $shop->slogan = $data['slogan'];
            $shop->description = $data['description'];
            if ($request->hasFile('photo')) {
                $shop->photo = $request->file('photo')->store(Shop::PHOTO_PATH);
            }
            $shop->is_active = true;

            $shop->save();

            DB::commit();
            return redirect()->back()
                ->with($this->success(__('alert.success.update')));
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return redirect()->back()
                ->withInput()
                ->with($this->error(__('alert.error.update')));
        }
    }
}