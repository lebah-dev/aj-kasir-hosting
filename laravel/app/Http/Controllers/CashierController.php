<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class CashierController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {
        return view('cashier.index');
    }

    public function print($encryptedCode)
    {
        $transaction = Transaction::query()->where('code', decrypt($encryptedCode))->firstOrFail();

        return view('cashier.print', [
            'data' => $transaction
        ]);
    }
}