<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class DataStorageController extends Controller
{
    /**
     * @param $encryptedPath
     * @return string
     */
    public function index($encryptedPath)
    {
        return Storage::get(decrypt($encryptedPath));
    }
}