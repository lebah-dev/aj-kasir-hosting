<?php

namespace App\Http\Controllers;

use App\DataTables\TransactionDataTable;
use App\Models\Expense;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TransactionController extends Controller
{
    /**
     * @return Factory|View
     */
    public function mostSalesProducts()
    {
        return view('transactions.most-sales-products', [
            'products' => Product::query()
                ->select('products.*', DB::raw('sum(transaction_items.qty) as sales'))
                ->join('transaction_items', 'products.id', '=', 'transaction_items.product_id')
                ->orderByDesc('sales')
                ->groupBy('products.id')
                ->get()
        ]);
    }

    /**
     * @return Factory|View
     */
    public function profitAndLoss(Request $request)
    {
        $startDate = $request->start_date ?? (new Carbon('first day of this month'))->format('Y-m-d');
        $endDate = $request->end_date ?? (new Carbon('last day of this month'))->format('Y-m-d');
        $category_id = $request->category_id;

        $transaction = Transaction::query()->whereHas('items');
        $expense = Expense::query();

        $transaction->whereDate('created_at', '>=', date('Y-m-d', strtotime($startDate)))
            ->whereDate('created_at', '<=', date('Y-m-d', strtotime($endDate)));
        $expense->whereDate('created_at', '>=', date('Y-m-d', strtotime($startDate)))
            ->whereDate('created_at', '<=', date('Y-m-d', strtotime($endDate)));

        if ($category_id) {
            $transaction->whereHas('items', function (Builder $item) use ($category_id) {
                $item->whereHas('product', function (Builder $product) use ($category_id) {
                    $product->where('category_id', $category_id);
                });
            });

            $expense->whereHas('product', function (Builder $product) use ($category_id) {
                $product->where('category_id', $category_id);
            });
        }

        return view('transactions.profit-and-loss', [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'category_id' => $category_id,
            'categories' => ProductCategory::query()->orderBy('name')->pluck('name', 'id'),

            'totalTransaction' => $transaction->count(),
            'totalTransactionItem' => $transaction->with('items')->get()
                ->sum(function ($transactions) {
                    return $transactions->items->sum('qty');
                }),
            'profit' => $transaction->sum('total_price'),

            'totalExpense' => $expense->count(),
            'totalExpenseItem' => $expense->sum('qty'),
            'loss' => $expense->sum('total_price')
        ]);
    }

    /**
     * Show list transaction page
     *
     * @param Request $request
     * @param TransactionDataTable $dataTable
     * @return mixed
     */
    public function manage(Request $request, TransactionDataTable $dataTable)
    {
        $result = $dataTable
            ->setStartEndDate($request->start_date, $request->end_date);

        if ($request->print == 'print') {
            return view('transactions.print', [
                'dates' => [
                    $request->start_date,
                    $request->end_date
                ],
                'data' => $result->query(new Transaction())->get()
            ]);
        }

        return $result->render('transactions.manage');
    }

    /**
     * Show transaction detail page
     *
     * @param int $id
     * @return Factory|View
     */
    public function show($id)
    {
        $transaction = Transaction::query()->findOrFail($id);
        return view('transactions.show', [
            'transaction' => $transaction
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();

            $transaction = Transaction::query()->find($id);
            if ($transaction) $transaction->delete();

            DB::commit();
            return redirect()->back()
                ->with($this->success(__('alert.success.delete')));
        } catch (\Exception $e) {
            DB::rollBack();
            report ($e);
            return redirect()->back()
                ->with($this->error(__('alert.error.delete')));
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id)
    {
        return view('transactions.edit', [
            'products' => Product::query()->orderBy('name')->get(),
            'transaction' => Transaction::query()->findOrFail($id),
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'cash' => 'required'
        ]);

        try {
            $cash = (int) str_replace(',', '', $request->cash);

            DB::beginTransaction();

            $transaction = Transaction::query()->findOrFail($id);
            $transaction->cash = $cash;
            $transaction->change = $cash - $transaction->total_price;
            $transaction->save();

            DB::commit();
            return redirect()->route('transactions.manage.show', $id)
                ->with($this->success(__('alert.success.update')));
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return redirect()->back()
                ->with($this->error(__('alert.error.update')));
        }
    }
}