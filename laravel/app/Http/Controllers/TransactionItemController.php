<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionItemController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add($id)
    {
        return view('transactions.items.add', [
            'products' => Product::query()
                ->select('products.*')
                ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
                ->orderBy('product_categories.name')
                ->orderby('products.name')
                ->get(),

            'transaction' => Transaction::query()->findOrFail($id)
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($id, Request $request)
    {
        $data = $request->validate([
            'product_id' => 'required',
            'qty' => 'required|min:1'
        ]);

        try {
            DB::beginTransaction();

            $transaction = Transaction::query()->find($id);
            if ($transaction == null) return redirect()->back()->withErrors([
                'transaction' => 'Transaksi tidak ditemukan'
            ]);

            $product = Product::query()->find($data['product_id']);
            if ($product == null) return redirect()->back()->withErrors([
                'product_id' => 'Produk tidak ditemukan'
            ]);

            if ($product->stock - $data['qty'] < 0) return redirect()->back()->withErrors([
                'qty' => 'Stok produk kurang. Stok saat ini adalah ' . $product->stock
            ]);

            $transactionItem = TransactionItem::query()
                ->where('transaction_id', $transaction->id)
                ->where('product_id', $product->id)
                ->first() ?: new TransactionItem([
                    'transaction_id' => $transaction->id,
                    'product_id' => $product->id
                ]);
            $transactionItem->qty += $data['qty'];
            $transactionItem->total_price = $product->price * $transactionItem->qty;
            $transactionItem->save();

            $product->stock -= $data['qty'];
            $product->save();

            $transaction = $this->updateTransaction($transaction);

            DB::commit();
            return redirect()->route('transactions.manage.edit', $transaction->id)
                ->with($this->success(__('alert.success.store')));
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return redirect()->back()
                ->with($this->error(__('alert.error.store') . ' : ' . $e->getMessage()));
        }
    }

    /**
     * @param $transactionId
     * @param $itemId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($transactionId, $itemId)
    {
        try {
            DB::beginTransaction();

            $transaction = Transaction::query()->find($transactionId);
            if ($transaction == null) return redirect()->back()->withErrors([
                'transaction' => 'Transaksi tidak ditemukan'
            ]);

            $transactionItem = $transaction->items()->find($itemId);
            if ($transactionItem == null) return redirect()->back()->withErrors([
                'transaction' => 'Transaksi item tidak ditemukan'
            ]);

            $transactionItem->product->stock += $transactionItem->qty;
            $transactionItem->product->save();
            $transactionItem->delete();

            $transaction = $this->updateTransaction($transaction);

            DB::commit();
            return redirect()->route('transactions.manage.edit', $transaction->id)
                ->with($this->success(__('alert.success.delete')));
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return redirect()->back()
                ->with($this->error(__('alert.error.delete') . ' : ' . $e->getMessage()));
        }
    }

    /**
     * @param $transaction
     * @return mixed
     */
    private function updateTransaction($transaction)
    {
        $transaction->total_price = $transaction->items->sum('total_price');
        $transaction->change = $transaction->cash - $transaction->total_price;
        $transaction->save();

        return $transaction;
    }
}