<?php

namespace App\DataTables;

use App\Models\ProductCategory;
use Yajra\DataTables\Html\Column;
use App\DataTables\DataTable;

class ProductCategoryDataTable extends DataTable
{
    /**
     * @param array $params
     * @return array
     */
    public function getActionRoutes($params)
    {
        return [
            'id' => $params[0],
            'edit' => route('product_categories.edit', $params),
            'delete' => route('product_categories.delete', $params)
        ];
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('products', function (ProductCategory $productCategory) {
                return $productCategory->products->count();
            })
            ->addColumn('action', function (ProductCategory $productCategory) {
                return view('layouts.actions', $this->getActionRoutes([$productCategory->id]))->render();
            })
            ->editColumn('updated_at', function (ProductCategory $productCategory) {
                return $productCategory->updated_at ?? $productCategory->created_at;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ProductCategory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductCategory $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('product-category-table')
                    ->addTableClass('w-100')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom($this->getDom())
                    ->orderBy(1)
                    ->buttons($this->getButtons(route('product_categories.create')));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->title('Nama'),
            Column::computed('products')->title('Jumlah Produk'),
            Column::make('updated_at')->title('Diperbaharui pada')->width(250),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(1)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ProductCategory_' . date('YmdHis');
    }
}
