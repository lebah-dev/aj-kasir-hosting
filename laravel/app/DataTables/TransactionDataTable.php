<?php

namespace App\DataTables;

use App\Models\Transaction;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use App\DataTables\DataTable;

class TransactionDataTable extends DataTable
{
    private $startDate;
    private $endDate;

    /**
     * @param $startDate
     * @param $endDate
     * @return $this
     */
    public function setStartEndDate($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getActionRoutes($params)
    {
        return [
            'id' => $params['id'],
            'show' => route('transactions.manage.show', $params['id']),
            'print' => route('cashier.print', encrypt($params['code'])),
            'edit' => route('transactions.manage.edit', $params['id']),
            'delete' => route('transactions.manage.delete', $params['id'])
        ];
    }

    /**
     * @param $createUrl
     * @return array
     */
    public function getButtons($createUrl)
    {
        $defaultClass = 'btn btn-sm btn-default rounded mb-2 ml-2';
        return [
            Button::make('copy')
                ->className($defaultClass),
            Button::make('excel')
                ->className($defaultClass),
            Button::raw('pdf')
                ->text('PDF')
                ->action("printPage()")
                ->className($defaultClass),
        ];
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function (Transaction $transaction) {
                return view('layouts.actions', $this->getActionRoutes([
                    'id' => $transaction->id,
                    'code' => $transaction->code
                ]))->render();
            })
            ->editColumn('total_price', function (Transaction $product) {
                return 'Rp. ' . number_format($product->total_price);
            })
            ->editColumn('created_at', function (Transaction $product) {
                return $product->created_at->format('Y-m-d H:i');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Transaction $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transaction $model)
    {
        $query = $model->newQuery()
            ->whereHas('items');

        if ($this->startDate && $this->endDate) {
            $query->whereDate('created_at', '>=', date('Y-m-d', strtotime($this->startDate)))
                ->whereDate('created_at', '<=', date('Y-m-d', strtotime($this->endDate)));
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('transaction-table')
                    ->addTableClass('w-100')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom($this->getDom())
                    ->orderBy(0)
                    ->buttons($this->getButtons(route('products.create')));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('created_at')->title('Tanggal'),
            Column::make('code')->title('Kode'),
            Column::make('total_price')->title('Total'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(1)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Transaction_' . date('YmdHis');
    }
}
