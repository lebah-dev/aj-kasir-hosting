<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable as BaseDataTable;

class DataTable extends BaseDataTable
{
    /**
     * @return string
     */
    public function getDom()
    {
        return
            "<'row mb-4 small'<'col text-md-right'B>>" .
            "<'row small'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" .
            "<'row small'<'col-sm-12'<'table-responsive'tr>>>" .
            "<'row small'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>" .
            "";
    }

    /**
     * @param $id
     * @return array
     */
    public function getActionRoutes($id)
    {
        return [
            'id' => $id,
            'show' => null,
            'edit' => null,
            'delete' => null
        ];
    }

    /**
     * @param $createUrl
     * @return array
     */
    public function getButtons($createUrl)
    {
        $defaultClass = 'btn btn-sm btn-default rounded mb-2 ml-2';
        return [
            Button::make('copy')
                ->className($defaultClass),
            Button::make('excel')
                ->className($defaultClass),
            Button::make('pdf')
                ->className($defaultClass),
            Button::raw('create')
                ->text('<i class="fa fa-plus"></i><span class="ml-2">Tambah</span>')
                ->action('location.href = "' . $createUrl . '"')
                ->className('btn btn-success rounded mb-2 ml-2')
        ];
    }
}