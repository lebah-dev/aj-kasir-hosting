<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Html\Column;
use App\DataTables\DataTable;

class ProductDataTable extends DataTable
{
    /**
     * @param array $params
     * @return array
     */
    public function getActionRoutes($params)
    {
        return [
            'id' => $params[0],
            'edit' => route('products.edit', $params),
            'delete' => route('products.delete', $params)
        ];
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function (Product $product) {
                return view('layouts.actions', $this->getActionRoutes([$product->id]))->render();
            })
            ->editColumn('updated_at', function (Product $product) {
                return $product->updated_at ?? $product->created_at;
            })
            ->addColumn('category', function (Product $product) {
                return $product->category->name ?? '-';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('product-table')
                    ->addTableClass('w-100')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom($this->getDom())
                    ->orderBy(1)
                    ->buttons($this->getButtons(route('products.create')));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('category')->title('Kategori'),
            Column::make('name')->title('Nama'),
            Column::make('price')->title('Harga'),
            Column::make('size')->title('Ukuran'),
            Column::make('weight')->title('Berat'),
            Column::make('stock')->title('stock'),
            Column::make('updated_at')->title('Diperbaharui pada')->width(250),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(1)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Product_' . date('YmdHis');
    }
}
