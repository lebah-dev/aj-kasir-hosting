<?php

namespace App\DataTables;

use App\Models\Expense;
use Yajra\DataTables\Html\Column;
use App\DataTables\DataTable;

class ExpenseDataTable extends DataTable
{
    /**
     * @param array $params
     * @return array
     */
    public function getActionRoutes($params)
    {
        return [
            'id' => $params[0],
            'delete' => route('expenses.delete', $params)
        ];
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function (Expense $expense) {
                return view('layouts.actions', $this->getActionRoutes([$expense->id]))->render();
            })
            ->editColumn('price', 'Rp. {{ number_format($price) }}')
            ->editColumn('total_price', 'Rp. {{ number_format($total_price) }}')
            ->editColumn('created_at', function (Expense $expense) {
                return $expense->created_at->format('Y-m-d H:i');
            })
            ->addColumn('product', function (Expense $expense) {
                return $expense->product->name ?? '-';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Expense $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Expense $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('expense-table')
                    ->addTableClass('w-100')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom($this->getDom())
                    ->orderBy(1)
                    ->buttons($this->getButtons(route('expenses.create')));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('product')->title('Produk'),
            Column::make('qty')->title('Jumlah'),
            Column::make('price')->title('Harga'),
            Column::make('total_price')->title('Sub Total'),
            Column::make('created_at')->title('Tanggal'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Expense_' . date('YmdHis');
    }
}
