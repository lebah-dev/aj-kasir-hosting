<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * @var string
     */
    protected $table = 'transactions';

    /**
     * @var array
     */
    protected $fillable = [
        'code', 'total_price', 'cash', 'change'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * @param null $date
     * @return string
     */
    public function getNewCode($date = null)
    {
        $lastData = $this->newQuery()
            ->orderByDesc('created_at')
            ->first();

        $sequence = sprintf('%04d', 1);
        if ($lastData) {
            $codes = explode('-', $lastData->code);
            if (isset($codes[1]) && isset($codes[2])) {
                if ($codes[1] == $lastData->created_at->format('Ymd')) {
                    $sequence = sprintf('%04d', ((int) $codes[2]) + 1);
                }
            };
        }

        $formattedDate = date('Ymd', strtotime($date ?? 'now'));
        return 'INV-' . $formattedDate . '-' . sprintf('%04d', $sequence);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(TransactionItem::class, 'transaction_id');
    }

    /**
     * @return mixed
     */
    public function getTotalProfit()
    {
        return $this->items->sum(function ($item) {
            return $item->getProfit();
        });
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param null $categoryId
     * @return Builder
     */
    public static function reportQuery($startDate, $endDate, $categoryId = null)
    {
        $transaction = static::query()->whereHas('items')
            ->whereDate('created_at', '>=', date('Y-m-d', strtotime($startDate)))
            ->whereDate('created_at', '<=', date('Y-m-d', strtotime($endDate)));

        if ($categoryId) {
            $transaction->whereHas('items', function (Builder $item) use ($categoryId) {
                $item->whereHas('product', function (Builder $product) use ($categoryId) {
                    $product->where('category_id', $categoryId);
                });
            });
        }

        return $transaction;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param null $categoryId
     * @return mixed
     */
    public static function getSales($startDate, $endDate, $categoryId = null)
    {
        return static::reportQuery($startDate, $endDate, $categoryId)
            ->with('items')->get()
            ->sum(function ($transactions) {
                return $transactions->items->sum('qty');
            });
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param null $categoryId
     * @return mixed
     */
    public static function getProfit($startDate, $endDate, $categoryId = null)
    {
        return static::reportQuery($startDate, $endDate, $categoryId)
            ->get()->sum(function (Transaction $transaction) {
                return $transaction->total_price;
            });
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param null $categoryId
     * @return mixed
     */
    public static function getNetProfit($startDate, $endDate, $categoryId = null)
    {
        return static::reportQuery($startDate, $endDate, $categoryId)
            ->get()->sum(function (Transaction $transaction) {
                return $transaction->getTotalProfit();
            });
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getData($startDate, $endDate)
    {
        return static::reportQuery($startDate, $endDate)
            ->with('items')
            ->get();
    }
}