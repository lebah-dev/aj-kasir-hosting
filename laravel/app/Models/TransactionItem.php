<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'transaction_items';

    /**
     * @var array
     */
    protected $fillable = [
        'transaction_id', 'product_id', 'qty', 'total_price'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getProfit()
    {
        return $this->total_price - ($this->qty * ($this->product->getAverageBuyPrice())) ?? 0;
    }
}