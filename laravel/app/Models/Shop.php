<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    const PHOTO_PATH = 'shop/photos';

    protected $table = 'shops';

    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'address',
        'slogan',
        'description',
        'photo',
    ];

    public function getPhotoAttribute($value)
    {
        return $value
            ? route('data-storage', encrypt($value))
            : null;
    }

    public static function get()
    {
        return static::query()->where('is_active', true)->first();
    }
}