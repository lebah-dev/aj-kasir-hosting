<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    /**
     * @var string
     */
    const IMAGE_PATH = 'products/images';

    /**
     * Model's table
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Model's fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category_id', 'price', 'size', 'weight', 'description', 'image', 'stock'
    ];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id', 'id');
    }

    /**
     * @return int
     */
    public function getAverageBuyPrice()
    {
        return $this->expenses->average('price') ?? 0;
    }

    /**
     * @return HasMany
     */
    public function expenses()
    {
        return $this->hasMany(Expense::class, 'product_id');
    }

    /**
     * @return HasMany
     */
    public function transactionItems()
    {
        return $this->hasMany(TransactionItem::class, 'product_id');
    }

    /**
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $value
            ? route('data-storage', encrypt($value))
            : '';
    }
}