<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Expense extends Model
{
    /**
     * @var string
     */
    protected $table = 'expenses';

    /**
     * @var array
     */
    protected $fillable = [
        'product_id', 'price', 'qty', 'total_price'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime'
    ];

    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param null $categoryId
     * @return Builder
     */
    public static function reportQuery($startDate, $endDate, $categoryId = null)
    {
        $expense = static::query()
            ->whereDate('created_at', '>=', date('Y-m-d', strtotime($startDate)))
            ->whereDate('created_at', '<=', date('Y-m-d', strtotime($endDate)));

        if ($categoryId) {
            $expense->whereHas('product', function (Builder $product) use ($categoryId) {
                $product->where('category_id', $categoryId);
            });
        }

        return $expense;
    }

    public static function getExpense($startDate, $endDate, $categoryId = null)
    {
        return static::reportQuery($startDate, $endDate, $categoryId)
            ->sum('total_price');
    }
}