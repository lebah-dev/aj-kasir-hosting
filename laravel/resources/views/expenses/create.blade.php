@extends('layouts.app')
@section('title', 'Tambah Data Pengeluaran')

@section('contents')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Data Pengeluaran</h1>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('expenses.store') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="product_id" class="col-md-3 col-form-label">Produk</label>
                            <div class="col-md-9">
                                <select name="product_id" id="product_id" class="form-control">
                                    <option value="">-- Pilih Produk --</option>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}" {{ old('product_id') == $product->id ? 'selected' : '' }}>[{{ $product->category->name ?? '-' }}] {{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="qty" class="col-md-3 col-form-label">Jumlah</label>
                            <div class="col-md-4">
                                <input type="number" min="1" name="qty" id="qty" class="form-control" value="{{ old('qty') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-md-3 col-form-label">Harga Satuan</label>
                            <div class="col-md-4">
                                <input type="text" min="0" name="price" id="price" class="form-control currency-mask" value="{{ old('price') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection