@extends('layouts.app')

@section('title', 'Dashboard')

@section('contents')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-12 col-md-6 mb-4">
            <a class="btn btn-lg btn-danger w-100 h-100 text-right p-4" href="{{ route('transactions.manage.index') }}">
                <h1><i class="fa fa-shopping-cart"></i></h1>
                <h2>Kelola Penjualan</h2>
            </a>
        </div>
        <div class="col-12 col-md-6 mb-4">
            <a class="btn btn-lg btn-warning w-100 h-100 text-right p-4" href="{{ route('settings.shop') }}">
                <h1><i class="fa fa-cogs"></i></h1>
                <h2>Pengaturan</h2>
            </a>
        </div>
        <div class="col-12 col-md-6 mb-4">
            <a class="btn btn-lg btn-info w-100 h-100 text-right p-4" href="{{ route('reports.index') }}">
                <h1><i class="fa fa-book"></i></h1>
                <h2>Laporan</h2>
            </a>
        </div>
        <div class="col-12 col-md-6 mb-4">
            <a class="btn btn-lg btn-success w-100 h-100 text-right p-4" href="{{ route('cashier.index') }}">
                <h1><i class="fa fa-calculator"></i></h1>
                <h2>Kasir</h2>
            </a>
        </div>
    </div>
@endsection