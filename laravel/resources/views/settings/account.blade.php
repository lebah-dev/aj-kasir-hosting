@extends('layouts.app')
@section('title', 'Pengaturan Akun')

@section('contents')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Pengaturan Akun</h1>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('settings.account') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <label for="inputGambar" class="col-md-3 col-form-label">Gambar Profil</label>
                            <div class="col-md-9">
                                <img id="gambarPreview" src="{{ $user->avatar ?: asset('images/default-user.png') }}" class="img-crop" width="200" height="200" style="border-radius: 100px;">
                                <input type="file" data-preview="#gambarPreview" class="form-control-file mt-3" name="avatar">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputNama" class="col-md-3 col-form-label">Nama</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="inputNama" name="name" value="{{ $user->name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-md-3 col-form-label">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="inputEmail" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputNumber" class="col-md-3 col-form-label">Nomor Hp</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="inputNumber" name="phone_number" value="{{ $user->phone_number }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputLokasi" class="col-md-3 col-form-label">Lokasi</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="inputLokasi" name="address" value="{{ $user->address }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-md-3 col-form-label">Password Lama</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="inputPassword" name="password">
                                <span class="small text-muted">Biarkan kosong jika tidak ingin mengganti.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPasswordBaru" class="col-md-3 col-form-label">Password Baru</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="inputPasswordBaru" name="new_password">
                                <span class="small text-muted">Biarkan kosong jika tidak ingin mengganti.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary" data-toggle="button" aria-pressed="false">
                                    Update
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection