@extends('layouts.app')
@section('title', 'Pengaturan Toko')

@section('contents')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Pengaturan Toko</h1>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <label for="inputGambar" class="col-md-3 col-form-label">Gambar Toko</label>
                            <div class="col-md-9">
                                <img id="gambarPreview" src="{{ $shop->photo ?? asset('images/default-user.png') }}" class="img-crop" width="200" height="200" style="border-radius: 100px;">
                                <input type="file" data-preview="#gambarPreview" class="form-control-file mt-3" name="photo">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputNama" class="col-sm-3 col-form-label">Nama Toko</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputNama" name="name" value="{{ $shop->name ?? old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputEmail" name="email" value="{{ $shop->email ?? old('email') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputNumber" class="col-sm-3 col-form-label">Nomor Hp</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputNumber" name="phone_number" value="{{ $shop->phone_number ?? old('phone_number') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputLokasi" class="col-sm-3 col-form-label">Lokasi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputLokasi" name="address" value="{{ $shop->address ?? old('address') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-3 col-form-label">Slogan Toko</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputPassword" name="slogan" value="{{ $shop->slogan ?? old('slogan') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPasswordBaru" class="col-sm-3 col-form-label">Deskripsi Toko</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputPasswordBaru" name="description" value="{{ $shop->description ?? old('description') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary" data-toggle="button" aria-pressed="false">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection