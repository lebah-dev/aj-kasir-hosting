<div xmlns:wire="http://www.w3.org/1999/xhtml">
    <div x-data="{...initCashier(), items: {{ json_encode($transactionItems) }}}">
        
        <div class="row">
            <div class="col-md-7 col-lg-8">
    
                <div class="row mb-4">
                    <div class="col">
                        <div class="card shadow-sm">
                            <div class="card-body pb-1">
                                <form wire:submit.prevent="$refresh">
                                    <div class="form-group row">
                                        <div class="col-sm-8 mb-2 mb-md-0">
                                            <input type="text" class="form-control form-control-sm" wire:model.debounce.500ms="search">
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-sm btn-success btn-block">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col">
    
                        <div class="row">
                            @foreach ($products as $product)
                            <div class="col-12 col-lg-4">
                                <div class="card mb-4 shadow-sm "
                                     wire:click="addItem({{ $product->id }})"
                                     @click="toggleAddItemModal">
                                    <div class="card-img-top rounded">
                                        <img class="img-crop rounded-top w-100" height="125"
                                            src="{{ $product->image }}" alt="{{ $product->image }}">
                                    </div>
                                    <div class="card-body p-md-2 rounded-0">
                                        <p class="mb-0"><span class="badge badge-dark">{{ $product->category->name ?? '' }}</span></p>
                                        <p class="mb-2">{{ $product->name }}</p>
                                        <h5 class="mb-0 text-right">Rp. {{ number_format($product->price) }}</h5>
                                    </div>
                                    <button 
                                        class="btn btn-block btn-sm btn-primary" 
                                        wire:click="addItem({{ $product->id }})"
                                    ><i class="fa fa-plus"></i> Tambah</button>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col">
                                {{ $products->links() }}
                            </div>
                        </div>
    
                    </div>
                </div>
    
            </div>
    
            <div class="col">
                <div class="card shadow-sm">
                    <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">Transaksi</h5>
                    </div>
                    <table class="table">
                        @isset($transactionItems)
                            @foreach ($transactionItems as $item)
                                <tr x-bind="showPaidButton = true" class="animate__animated animate__fadeIn">
                                    <td class="align-middle" style="width: 1px;">
                                        <a role="button" class="btn btn-link text-danger p-0" wire:click="removeItem({{ $item['product_id'] }})"><i class="fa fa-trash"></i></a>
                                    </td>
                                    <td class="small">
                                        <p class="mb-0">{{ $item['product_name'] }}</p>
                                        <p class="mb-0">
                                            <span class="float-left">{{ $item['quantity'] }}x</span>
                                            <span class="float-right">@Rp. {{ number_format($item['product_price']) }}</span>
                                            <span class="clearfix"></span>
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        @endisset
                        <tr>
                            <td colspan="2">
                                <p class="mb-0">Total</p>
                                <h4 class="text-right">Rp. {{ number_format($totalTransaction) }}</h4>
                            </td>
                        </tr>
                    </table>
                    <template x-if="items.length > 0">
                        <div class="card-footer animate__animated animate__fadeIn">
                            <button class="btn btn-success btn-sm btn-block" @click="toggleMakePaymentModal">Bayar</button>
                        </div>
                    </template>
                </div>
            </div>
        </div>

        <div x-show.transition.opacity="showAddItemModal" class="modal" tabindex="-1" role="dialog">
            <div class="overlay" @click="toggleAddItemModal" wire:click="initNewItem"></div>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Produk Detail</h5>
                        <button type="button" class="close" @click="toggleAddItemModal" wire:click="initNewItem">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-4">
                            <div class="col-sm-4">
                                <img class="img-crop rounded w-100 shadow-sm"
                                     src="{{ $newItem['product_image'] }}" alt="">
                            </div>
                            <div class="col-sm-8">
                                <p class="mb-0">{{ $newItem['product_name'] ?? '' }}</p>
                                <p class="small">
                                    <span class="badge badge-dark shadow-sm">{{ $newItem['product_category'] ?? '' }}</span>
                                    <span class="badge badge-light shadow-sm">ukuran : {{ $newItem['product_size'] ?? '' }}</span>
                                    <span class="badge badge-light shadow-sm">berat : {{ $newItem['product_weight'] ?? '' }}</span>
                                </p>
                                <p class="small">
                                    <span class="mr-4">Stock: {{ $newItem['product_stock'] }}</span>
                                    <span>@Rp. {{ number_format($newItem['product_price'] ?? 0) }}</span>
                                </p>
                                <p class="small mb-0">{{ $newItem['product_description'] ?? '' }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Jumlah</label>
                            <div class="input-group">
                                <input autofocus id="quantity" type="number" min="1" max="{{ $newItem['product_stock'] }}" class="form-control" name="quantity"
                                wire:model="newItem.quantity">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" wire:click="addQuantity"><i class="fa fa-plus"></i></button>
                                    <button class="btn btn-outline-secondary" type="button" wire:click="substractQuantity"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p class="mb-0">Total</p>
                            <h5 class="text-right">Rp. {{ number_format($newItem['product_price'] * $newItem['quantity']) }}</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" @click="toggleAddItemModal()" wire:click="storeItem">Tambah Item</button>
                        <button type="button" class="btn btn-secondary" @click="toggleAddItemModal()" wire:click="initNewItem">Batal</button>
                    </div>
                </div>
            </div>
        </div>

        <div x-show.transition.opacity="showMakePaymentModal" class="modal" tabindex="-1" role="dialog">
            <div class="overlay" @click="toggleMakePaymentModal" wire:click="initNewItem"></div>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pembayaran</h5>
                        <button type="button" class="close" @click="toggleMakePaymentModal" wire:click="initNewItem">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="total_transaction">Total Pembayaran</label>
                            <h4>Rp. {{ number_format($totalTransaction) }}</h4>
                        </div>
                        <div class="form-group">
                            <label for="cash">Jumlah Bayar</label>
                            <input autofocus autocomplete="off" id="cash" type="text" min="1" class="form-control currency-mask" name="cash" wire:model="cash" wire:keyup="countChange">
                        </div>
                        <div class="form-group">
                            <label for="change">Jumlah Kembalian</label>
                            <h4>Rp. {{ number_format($change) }}</h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" @click="toggleMakePaymentModal(); togglePostPaymentModal();" wire:click="makePayment">Bayar</button>
                        <button type="button" class="btn btn-secondary" @click="toggleMakePaymentModal" wire:click="initNewItem">Batal</button>
                    </div>
                </div>
            </div>
        </div>

        <div x-show.transition.opacity="showPostPaymentModal" class="modal" tabindex="-1" role="dialog">
            <div class="overlay" @click="location.reload()"></div>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Struk Pembayaran</h5>
                        <button type="button" class="close" @click="togglePostPaymentModal" wire:click="initNewItem">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="print" style="font-family: monospace;">
                        <table class="table animate__animated animate__fadeIn">
                            <tr>
                                <td class="small text-center">
                                    <p class="mb-0 font-weight-bold">{{ Shop::get()->name ?? '' }}</p>
                                    <p class="mb-0">{{ Shop::get()->address ?? ''}}</p>
                                    <p class="mb-0">{{ Shop::get()->phone_number ?? '' }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="small">
                                    <table class="w-100">
                                        <tr><td class="border-0 p-0">Tanggal</td><td class="border-0 p-0">:</td><td class="border-0 p-0">{{ date('d-m-Y', strtotime($transactionDate)) }}</td></tr>
                                        <tr><td class="border-0 p-0">Waktu</td><td class="border-0 p-0">:</td><td class="border-0 p-0">{{ date('H:m', strtotime($transactionDate)) }} {{ config('kasir.timezone', 'WIB') }}</td></tr>
                                        <tr><td class="border-0 p-0">Nomor</td><td class="border-0 p-0">:</td><td class="border-0 p-0">{{ $transactionCode }}</td></tr>
                                    </table>
                                </td>
                            </tr>
                            @isset($transactionItems)
                                @foreach ($transactionItems as $item)
                                    <tr>
                                        <td class="small">
                                            <p class="mb-0">{{ $item['product_name'] }}</p>
                                            <p class="mb-0">
                                                <span class="float-left">{{ $item['quantity'] }}x</span>
                                                <span class="float-right">{{ number_format($item['product_price']) }}</span>
                                                <span class="clearfix"></span>
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset
                            <tr>
                                <td class="small">
                                    <p class="mb-0">
                                        <span class="float-left">Total</span>
                                        <span class="float-right">{{ number_format($totalTransaction) }}</span>
                                        <span class="clearfix"></span>
                                    </p>
                                    <p class="mb-0">
                                        <span class="float-left">Bayar</span>
                                        <span class="float-right">{{ number_format( (int) str_replace(',', '', $cash)) }}</span>
                                        <span class="clearfix"></span>
                                    </p>
                                    <p class="mb-0">
                                        <span class="float-left">Kembali</span>
                                        <span class="float-right">{{ number_format($change) }}</span>
                                        <span class="clearfix"></span>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="small">
                                    <p class="mb-0 text-center">Terima Kasih atas Kunjungannya</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="print();">Print</button>
                        {{-- <a href="{{ $printRoute }}" target="_blank" class="btn btn-primary" @click="location.reload()">Print</a> --}}
                        <button type="button" class="btn btn-primary" @click="location.reload()">Selesai</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function initCashier() {
            return {
                toggleBody(modal) {
                    if (modal) {
                        $('body').css('overflow', 'hidden');
                    } else {
                        $('body').css('overflow', 'inherit')
                    }
                },

                showAddItemModal: false,
                toggleAddItemModal() {
                    this.showAddItemModal = !this.showAddItemModal;
                    this.toggleBody(this.showAddItemModal);
                },

                showMakePaymentModal: false,
                toggleMakePaymentModal() {
                    this.showMakePaymentModal = !this.showMakePaymentModal;
                    this.toggleBody(this.showMakePaymentModal);
                },

                showPostPaymentModal: false,
                togglePostPaymentModal() {
                    this.showPostPaymentModal = !this.showPostPaymentModal;
                    this.toggleBody(this.showPostPaymentModal);
                }
            }
        }
    </script>
</div>
