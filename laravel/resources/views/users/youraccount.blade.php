<!doctype html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Your Account</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- Custom fonts for this template-->
    <link href="{{ asset('sb-admin-2/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    
    <!-- Custom styles for this template-->
    <link href="{{ asset('sb-admin-2/css/sb-admin-2.min.css') }}" rel="stylesheet">

  </head>
  <body>
  <body>
    <div class="fixed-top">
      <div class="collapse" id="navbarToggleExternalContent">

        <div class="bg-dark p-4">
          <h5 class="text-white h4">Collapsed content</h5>
          <span class="text-muted">Toggleable via the navbar brand.</span>
        </div>
      </div>
  
      <nav class="navbar navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <i class="fas fa-wallet text-white float-center"> Kasir AJ's</i>
      </nav>
    </div>

    <div class="row no-gutters mt-5">
      <div class="col-md-2 bg-warning mt-2 pr-3 pt-4 ">
        
          <ul class="nav flex-column ml-3 mb-5 ">
          <i class="fa-5x fas fa-hand-holding-usd text-white float-center "></i>
              <li class="nav-item mt-4">
                <a class="nav-link active text-white float-center" href="#">Akun Anda</a><hr class="bg-secondary">
              </li>
              <li class="nav-item">
                <a class="nav-link text-white float-center" href="#">Pengaturan Toko</a><hr class="bg-secondary">
              </li>
              <li class="nav-item">
                  <a class="nav-link text-white float-center" href="#">Keluar</a><hr class="bg-secondary">
              </li>
              <li class="nav-item">
                  <a class="nav-link text-white float-right " href="#">Kembali</a>
              </li>
          </ul>
        </div>
          
      </div>


      <div class="col-md-10 ">
        <div class="media mr-5 pl-3 pt-3">
          <img src="..." class="align-self-center mr-3" alt="...">
          <div class="media-body">
            <h5 class="mt-0 float-right">Akun Anda</h5>
          </div>
        </div>


        <form class="pl-3 pt-3 mr-3">
          <div class="form-group row">
            <label for="Nama" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="text">
            </div>
          </div>
          <div class="form-group row">
            <label for="Email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="text">
            </div>
          </div>
          <div class="form-group row">
            <label for="number" class="col-sm-2 col-form-label">No. Hp</label>
            <div class="col-sm-10">
              <input type="number" class="form-control" id="number">
            </div>
          </div>
          <div class="form-group row">
            <label for="lokasi" class="col-sm-2 col-form-label">lokasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="text">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Password Lama</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="inputPassword">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Password Baru</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="inputPassword">
            </div>
          </div>
        </form>
      </div>
    </div>


    



  <!-- Bootstrap core JavaScript-->
<script src="{{ asset('sb-admin-2/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('sb-admin-2/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('sb-admin-2/js/sb-admin-2.min.js') }}"></script>

  </body>
</html>