@extends('layouts.auth')
@section('title', 'Login')

@section('contents')
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-4 col-lg-6 col-md-8 col-12 py-5">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="text-primary"><i class="fas fa-sign-in-alt"></i></h1>
                                        <h4 class="text-gray-900 mb-4">Login</h4>
                                    </div>
                                    <form class="user" action="{{ route('login') }}" method="post"> @csrf
                                        @if (session('message'))
                                            <div class="text-center mb-4">
                                                <span class="d-block text-info small">{{ session('message') }}</span>
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                            <div class="text-center mb-4">
                                                @foreach ($errors->all() as $error)
                                                    <span class="d-block text-danger small">{{ $error }}</span>
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" aria-label="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" aria-label="exampleInputPassword" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck" name="remember_me">
                                                <label class="custom-control-label" for="customCheck">Remember Me</label>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </button>
                                        <hr>
                                    </form>
                                    <div>
                                        <a class="small float-left" href="{{ route('password.request') }}">Forgot Password?</a>
                                        <a class="small float-right" href="{{ route('register') }}">Create an Account!</a>
                                        <span class="clearfix"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection