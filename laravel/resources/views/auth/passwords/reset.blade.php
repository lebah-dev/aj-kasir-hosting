@extends('layouts.auth')
@section('title', 'Reset Password')

@section('contents')
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-lg-6 col-md-8 col-12 py-5">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="text-warning"><i class="fas fa-user-lock"></i></h1>
                                        <h4 class="text-gray-900 mb-4">Reset Password</h4>
                                    </div>
                                    <form class="user" action="{{ route('password.update') }}" method="post"> @csrf
                                        @if (session('status'))
                                            <div class="text-center mb-4">
                                                <span class="d-block text-success small">{{ session('status') }}</span>
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                            <div class="text-center mb-4">
                                                @foreach ($errors->all() as $error)
                                                    <span class="d-block text-danger small">{{ $error }}</span>
                                                @endforeach
                                            </div>
                                        @endif

                                        <input type="hidden" name="token" value="{{ $token }}">

                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <input type="email" class="form-control form-control-user" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus aria-label="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" aria-label="inputPassword" placeholder="Password">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password" aria-label="inputPasswordConfirmation" placeholder="Password Confirmation">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col">
                                                <button type="submit" class="btn btn-warning btn-user btn-block">
                                                    Reset Password
                                                </button>
                                            </div>
                                        </div>
                                        <hr>
                                    </form>
                                    <div class="text-center">
                                        <a class="small text-warning" href="{{ route('login') }}">Back to Login Page.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection
