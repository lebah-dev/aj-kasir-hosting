@extends('layouts.auth')
@section('title', 'register')

@section('contents')
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-lg-6 col-md-8 col-12 py-md-5 py-3">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="text-success"><i class="fas fa-user-check"></i></h1>
                                        <h4 class="text-gray-900 mb-4">Register</h4>
                                    </div>
                                    <form class="user" action="{{ route('register') }}" method="post"> @csrf
                                        @if($errors->any())
                                            <div class="row">
                                                <div class="col">
                                                    <div class="mb-4">
                                                        @foreach($errors->all() as $error)
                                                            <span class="d-block small text-danger text-center">{{ $error }}</span>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" aria-label="inputName" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" aria-label="inputEmail" placeholder="Enter Email Address">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" aria-label="inputPassword" placeholder="Password">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password_confirmation" aria-label="inputPasswordConfirmation" placeholder="Password Confirmation">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <button type="submit" class="btn btn-success btn-user btn-block">
                                                    Register
                                                </button>
                                            </div>
                                        </div>
                                        <hr>
                                    </form>
                                    <div class="text-center">
                                        <a class="small text-success" href="{{ route('login') }}">Have an account? Login here.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection