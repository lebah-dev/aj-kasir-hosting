@extends('layouts.app')
@section('title', 'Detail Transaksi')

@section('contents')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800">Detail Transaksi</h1>
        <p class="text-right">
            <a href="{{ route('transactions.manage.index') }}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
            <a href="{{ route('cashier.print', encrypt($transaction->code)) }}" target="_blank" class="btn btn-sm btn-dark"><i class="fa fa-print"></i></a>
        </p>
    </div>

    <div class="row">
        <div class="col-md-7 mb-4 mb-md-0">
            <div class="card">
                <div class="card-header">
                    <h6 class="card-title mb-0">{{ $transaction->items->count() }} Item</h6>
                </div>
                <table class="table small">
                    @foreach($transaction->items as $item)
                        <tr>
                            <td style="width: 100px;">
                                <img src="{{ $item->product->image }}" alt="{{ $item->product->name }}" width="100" class="img-crop rounded shadow-sm">
                            </td>
                            <td>
                                <p class="mb-0">{{ $item->product->name }}</p>
                                <p>{{ $item->qty }} x Rp. {{ number_format($item->product->price) }}</p>
                                <h5>Rp. {{ number_format($item->total_price) }}</h5>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <table class="table table-borderless mb-0 small">
                    <tr class="bg-light"><td colspan="3" class="border-bottom"><span class="h6 card-title mb-0">Detail Transaksi</span></td></tr>
                    <tr><td>Kode</td><td>:</td><td>{{ $transaction->code }}</td></tr>
                    <tr><td>Tanggal</td><td>:</td><td>{{ date('d-m-Y', strtotime($transaction->created_at)) }}</td></tr>
                    <tr><td>Waktu</td><td>:</td><td>{{ date('H:m', strtotime($transaction->created_at)) }} {{ config('kasir.timezone') }}</td></tr>
                    <tr class="bg-light"><td colspan="3" class="border-bottom border-top"><span class="h6 card-title mb-0">Detail Pembayaran</span></td></tr>
                    <tr><td>Total Transaksi</td><td>:</td><td>Rp. {{ number_format($transaction->total_price) }}</td></tr>
                    <tr><td>Bayar</td><td>:</td><td>Rp. {{ number_format($transaction->cash) }}</td></tr>
                    <tr><td>Kembali</td><td>:</td><td>Rp. {{ number_format($transaction->change) }}</td></tr>
                </table>
            </div>
        </div>
    </div>
@endsection