@extends('layouts.app')
@section('title', 'Rugi Laba')

@section('contents')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Rekapitulasi Laporan Rugi Laba</h1>
    </div>

    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body pb-0">
                    <form action="" method="get">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb-0">Periode</label>
                                    <input type="date" name="start_date" class="form-control form-control-sm" value="{{ $start_date }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb-0">Sampai</label>
                                    <input type="date" name="end_date" class="form-control form-control-sm" value="{{ $end_date }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="categoryIdInput">Kategori Produk</label>
                            <select name="category_id" id="categoryIdInput" class="form-control">
                                <option value="">Semua Kategori</option>
                                @foreach($categories as $id => $name)
                                    <option value="{{ $id }}" {{ $category_id == $id ? 'selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-primary">Terapkan Filter</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-5">
                        <div class="col-md-6">
                            <h5 class="mb-3">Pendapatan</h5>
                            <table class="w-100">
                                <tr><td>Jumlah Transaksi</td><td>:</td><td>{{ $totalTransaction ?? 0 }} Transaksi</td></tr>
                                <tr><td>Jumlah Item Terjual</td><td>:</td><td>{{ $totalTransactionItem ?? 0 }} Item</td></tr>
                                <tr><td>Total</td><td>:</td><td>Rp. {{ number_format($profit) }}</td></tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <h5 class="mb-3">Pengeluaran</h5>
                            <table class="w-100">
                                <tr><td>Jumlah Pengeluaran</td><td>:</td><td>{{ $totalExpense ?? 0 }} Transaksi</td></tr>
                                <tr><td>Jumlah Item Dibeli</td><td>:</td><td>{{ $totalExpenseItem ?? 0 }} Item</td></tr>
                                <tr><td>Total</td><td>:</td><td>Rp. {{ number_format($loss) }}</td></tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <table class="w-100">
                                <tr><td class="h4">Grand Total</td><td class="h4 text-right">Rp. {{ number_format($profit - $loss) }}</td></tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection