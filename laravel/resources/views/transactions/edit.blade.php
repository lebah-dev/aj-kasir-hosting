@extends('layouts.app')
@section('title', 'Koreksi Transaksi')

@section('contents')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800">Koreksi Transaksi</h1>
        <p class="text-right">
            <a href="{{ route('transactions.manage.index') }}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
        </p>
    </div>

    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('transactions.manage.update', $transaction->id) }}" method="post">@csrf @method('put')
                        <div class="form-group row">
                            <label class="col-md-3">Kode</label>
                            <div class="col-md-4">
                                <span>{{ $transaction->code }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3">Total Harga</label>
                            <div class="col-md-4">
                                <span>Rp. {{ number_format($transaction->total_price) }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cashInput" class="col-md-3">Bayar</label>
                            <div class="col-md-4">
                                <input id="cashInput"
                                       type="text"
                                       class="form-control currency-mask"
                                       min="1"
                                       name="cash"
                                       value="{{ $transaction->cash }}"
                                >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3">Kembali</label>
                            <div class="col-md-4">
                                <span>Rp. {{ number_format($transaction->change) }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">Koreksi Transaksi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="text-right mb-4">
                        <a href="{{ route('transactions.manage.items.add', $transaction->id) }}" role="button" class="btn btn-sm btn-primary">Tambah Item</a>
                    </div>
                    <table class="table small">
                        <thead>
                            <tr>
                                <td>Kategori</td>
                                <td>Produk</td>
                                <td>Total Harga</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transaction->items as $item)
                                <tr>
                                    <td>{{ $item->product->category->name ?? '-' }}</td>
                                    <td>
                                        <p class="mb-0">{{ $item->product->name }}</p>
                                        <p class="mb-0">
                                            <span>{{ $item->qty }} x Rp. {{ number_format($item->product->price) }}</span>
                                        </p>
                                    <td>Rp. {{ number_format($item->total_price) }}</td>
                                    <td>
                                        <form action="{{ route('transactions.manage.items.delete', [$transaction->id, $item->id]) }}" method="post">
                                            @csrf @method('delete')
                                            <button type="submit" class="btn btn-link btn-sm text-danger"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection