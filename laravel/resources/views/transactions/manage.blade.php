@extends('layouts.app')
@section('title', 'Kelola Penjualan')

@push('styles')
    <link href="{{ asset('sb-admin-2/vendor/datatables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('contents')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Penjualan</h1>
    </div>

    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body pb-0">
                    <form action="" method="get">
                        <div class="form-group row">
                            <div class="col-md-4 mb-2">
                                <label class="mb-0">Periode</label>
                                <input type="date" name="start_date" class="form-control form-control-sm" value="{{ request('start_date') }}">
                            </div>
                            <div class="col-md-4 mb-2">
                                <label class="mb-0">Sampai</label>
                                <input type="date" name="end_date" class="form-control form-control-sm" value="{{ request('end_date') }}">
                            </div>
                            <div class="col-md-4 mb-2 mt-auto">
                                <button type="submit" class="btn btn-sm btn-primary">Cari Data Transaksi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                   {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('sb-admin-2/vendor/datatables/datatables.min.js') }}"></script>

    {{ $dataTable->scripts() }}

    <script>
        function printPage() {
            let url = document.location.href;
            if(document.location.href.toString().includes('?')) {
                url = document.location.href+"&print=print";
            }else{
                url = document.location.href+"?print=print";
            }

            window.open(url, '_blank');
        }
    </script>
@endpush