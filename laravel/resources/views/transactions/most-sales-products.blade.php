@extends('layouts.app')
@section('title', 'Barang Terlaku')

@section('contents')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Produk Terlaku</h1>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>No</td>
                            <td>Gambar</td>
                            <td>Nama Produk</td>
                            <td>Terjual</td>
                        </tr>
                        </thead>
                        <tbody>
                        @php $counter = 1; @endphp
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $counter++ }}</td>
                                <td>
                                    <img class="img-crop rounded shadow-sm" width="150" src="{{ $product->image }}" alt="{{ $product->image }}">
                                </td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->sales }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection