@extends('layouts.app')
@section('title', 'Tambah Item')

@section('contents')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h4 mb-0 text-gray-800">Tambah Item</h1>
        <p class="text-right">
            <a href="{{ route('transactions.manage.edit', $transaction->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
        </p>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('transactions.manage.items.store', $transaction->id) }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-3">Kode Transaksi</label>
                            <div class="col-md-9">
                                <p>{{ $transaction->code }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_id" class="col-form-label col-md-3">Product</label>
                            <div class="col-md-9">
                                <select name="product_id" id="product_id" class="form-control">
                                    <option value="">-- Pilih Produk --</option>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}">
                                            <span>[{{ $product->category->name ?? 'Tanpa Kategori' }}]</span>
                                            <span>{{ $product->name }}</span>
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="qty" class="col-form-label col-md-3">Jumlah</label>
                            <div class="col-md-3">
                                <input type="number" id="qty" name="qty" value="1" min="1" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary">Tambah Item</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection