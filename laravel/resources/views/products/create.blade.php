@extends('layouts.app')
@section('title', 'Tambah Produk')

@section('contents')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Produk</h1>
    </div>
    
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="" method="post" enctype="multipart/form-data"> @csrf
                        <div class="form-group row">
                            <label for="imageInput" class="col-12 col-md-4 col-lg-2 col-form-label">Gambar</label>
                            <div class="col-12 col-md-8 col-lg-6">
                                <input type="file"
                                       name="image"
                                       id="imageInput"
                                       class="form-control-file"
                                       data-preview="#imagePreview"
                                >
                                @error('image') <span class="text-danger small">{{ $message }}</span> @enderror
                                <div class="mt-2">
                                    <img id="imagePreview" class="img-crop rounded shadow-sm" width="150" src="" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="categoryIdInput" class="col-12 col-md-4 col-lg-2 col-form-label">Kategori Produk</label>
                            <div class="col-12 col-md-8 col-lg-6">
                                <select name="category_id" id="categoryIdInput" class="form-control @error('category_id') is-invalid @enderror">
                                    <option value=""> - </option>
                                    @foreach($categories as $id => $name)
                                        <option value="{{ $id }}" {{ old('category_id') == $id ? 'selected' : '' }}>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @error('category_id') <span class="text-danger small">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nameInput" class="col-12 col-md-4 col-lg-2 col-form-label">Nama Produk</label>
                            <div class="col-12 col-md-8 col-lg-6">
                                <input type="text" name="name" id="nameInput" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                                @error('name') <span class="text-danger small">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="priceInput" class="col-12 col-md-4 col-lg-2 col-form-label">Harga Satuan</label>
                            <div class="col-12 col-md-8 col-lg-4">
                                <input type="number" name="price" id="priceInput" class="form-control @error('price') is-invalid @enderror" min="0"  value="{{ old('price') }}">
                                @error('price') <span class="text-danger small">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sizeInput" class="col-12 col-md-4 col-lg-2 col-form-label">Ukuran Produk</label>
                            <div class="col-12 col-md-8 col-lg-2">
                                <input type="text" name="size" id="sizeInput" class="form-control @error('size') is-invalid @enderror" min="0"  value="{{ old('size') }}">
                                @error('size') <span class="text-danger small">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="weightInput" class="col-12 col-md-4 col-lg-2 col-form-label">Berat Produk</label>
                            <div class="col-12 col-md-8 col-lg-2">
                                <input type="text" name="weight" id="weightInput" class="form-control @error('weight') is-invalid @enderror" value="{{ old('weight') }}">
                                @error('weight') <span class="text-danger small">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descriptionInput" class="col-12 col-md-4 col-lg-2 col-form-label">Deskripsi</label>
                            <div class="col-12 col-md-8 col-lg-6">
                                <textarea name="description" id="descriptionInput" class="form-control  @error('description') is-invalid @enderror" rows="5">{{ old('description') }}</textarea>
                                @error('description') <span class="text-danger small">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="stockInput" class="col-12 col-md-4 col-lg-2 col-form-label">Stok Produk</label>
                            <div class="col-12 col-md-8 col-lg-2">
                                <input type="number" name="stock" id="stockInput" class="form-control @error('stock') is-invalid @enderror" min="0"  value="{{ old('stock') }}">
                                @error('stock') <span class="text-danger small">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12 col-md-4 col-lg-2 col-form-label"></div>
                            <div class="col-12 col-md-8 col-lg-6">
                                <button type="submit" class="btn btn-primary">Tambah</button>
                                <a href="{{ route('products.index') }}" class="btn btn-light ml-2">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection