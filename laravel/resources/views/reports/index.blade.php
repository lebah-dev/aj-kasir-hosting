@extends('layouts.app')
@section('title', 'Laporan')

@push('styles')
    <style>
        .report-icon {
            position: absolute;
            bottom: -11px;
            left: 10px;
            color: rgba(255, 255, 255, 0.3);
            font-size: 4rem;
        }
    </style>
@endpush

@section('contents')
    <div class="row">
        <div class="col-12">
            <div class="card bg-info p-3 mb-4">
                <div class="report-icon"><i class="fas fa-percent"></i></div>
                <table class="table-borderless text-white small w-100 mb-0">
                    <tr><td class="h5">Jam</td><td>{{ date('H:m') }} {{ config('kasir.timezone') }}</td></tr>
                    <tr><td class="h5">Penjualan</td><td>{{ $current['sales'] }} PRODUK</td></tr>
                    <tr><td class="h5">Profit</td><td>Rp. {{ number_format($current['profit']) }}</td></tr>
                    <tr><td colspan="2" class="h5 pt-5 text-right">SAAT INI</td></tr>
                </table>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="card bg-success p-3 mb-4">
                <div class="report-icon"><i class="fas fa-percent"></i></div>
                <table class="table-borderless text-white small w-100 mb-0">
                    <tr><td class="h6">Sales</td><td>{{ $daily['sales'] }} Produk</td></tr>
                    <tr><td class="h6">Profit</td><td>Rp. {{ number_format($daily['profit']) }}</td></tr>
                    <tr><td class="h6">Laba/Rugi</td><td>Rp. {{ number_format($daily['net_profit']) }}</td></tr>
                    <tr><td colspan="2" class="h6 pt-4 text-right">LAPORAN HARIAN</td></tr>
                    <tr><td colspan="2" class="pt-2 text-right"><a href="{{ route('reports.detail', 'daily') }}" class="btn-light btn-sm">Prin ke PDF</a></td></tr>
                </table>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="card bg-success p-3 mb-4">
                <div class="report-icon"><i class="fas fa-percent"></i></div>
                <table class="table-borderless text-white small w-100 mb-0">
                    <tr><td class="h6">Sales</td><td>{{ $weekly['sales'] }} Produk</td></tr>
                    <tr><td class="h6">Profit</td><td>Rp. {{ number_format($weekly['profit']) }}</td></tr>
                    <tr><td class="h6">Laba/Rugi</td><td>Rp. {{ number_format($weekly['net_profit']) }}</td></tr>
                    <tr><td colspan="2" class="h6 pt-4 text-right">LAPORAN Mingguan</td></tr>
                    <tr><td colspan="2" class="pt-2 text-right"><a href="{{ route('reports.detail', 'weekly') }}" class="btn-light btn-sm">Prin ke PDF</a></td></tr>
                </table>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="card bg-success p-3 mb-4">
                <div class="report-icon"><i class="fas fa-percent"></i></div>
                <table class="table-borderless text-white small w-100 mb-0">
                    <tr><td class="h6">Sales</td><td>{{ $monthly['sales'] }} Produk</td></tr>
                    <tr><td class="h6">Profit</td><td>Rp. {{ number_format($monthly['profit']) }}</td></tr>
                    <tr><td class="h6">Laba/Rugi</td><td>Rp. {{ number_format($monthly['net_profit']) }}</td></tr>
                    <tr><td colspan="2" class="h6 pt-4 text-right">LAPORAN Bulanan</td></tr>
                    <tr><td colspan="2" class="pt-2 text-right"><a href="{{ route('reports.detail', 'monthly') }}" class="btn-light btn-sm">Prin ke PDF</a></td></tr>
                </table>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="card bg-success p-3 mb-4">
                <div class="report-icon"><i class="fas fa-percent"></i></div>
                <table class="table-borderless text-white small w-100 mb-0">
                    <tr><td class="h6">Sales</td><td>{{ $annual['sales'] }} Produk</td></tr>
                    <tr><td class="h6">Profit</td><td>Rp. {{ number_format($annual['profit']) }}</td></tr>
                    <tr><td class="h6">Laba/Rugi</td><td>Rp. {{ number_format($annual['net_profit']) }}</td></tr>
                    <tr><td colspan="2" class="h6 pt-4 text-right">LAPORAN Tahunan</td></tr>
                    <tr><td colspan="2" class="pt-2 text-right"><a href="{{ route('reports.detail', 'annual') }}" class="btn-light btn-sm">Prin ke PDF</a></td></tr>
                </table>
            </div>
        </div>
    </div>
@endsection