<!doctype html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan</title>

    <link rel="stylesheet" href="{{ asset('css/tailwind.min.css') }}">

    <style>
        @page {
            size: A4;
            margin: 20mm;
        }
        /* print styles */
        @media print {
            body {
                visibility: hidden;
                margin: 0;
                padding: 0;
                color: #000;
                background-color: #fff;
            }
            .page {
                visibility: visible;
                box-shadow: none;
                margin: 0;
                padding: 0;
            }
        }

        @media screen {
            .page {
                width: 210mm;
                min-height: 297mm;
                padding: 20mm
            }
        }
    </style>
</head>
<body class="bg-gray-600">
    <div class="page bg-white mx-auto mt-12 shadow-2xl">

        <div class="text-center mb-6 pb-1 border-b-2 border-black">
            <h1 class="text-2xl font-bold">{{ Shop::get()->name ?? 'Nama Toko' }}</h1>
            <p class="pb-3 border-b-4 border-black">{{ Shop::get()->address ?? 'Jl. Alamat Lengkap Toko' }}</p>
        </div>

        <div class="text-center mb-10 font-bold">
            <h1 class="text-center text-xl">Laporan Penjualan</h1>
            <p class="text-sm">Periode {{ date('d M Y', strtotime($dates[0])) }} s/d {{ date('d M Y', strtotime($dates[1])) }}</p>
        </div>

        <table class="table-auto w-full text-xs mb-8">
            @foreach($data as $key => $datum)

                <tr class="text-left">
                    <th class="border border-black px-2 py-1">#</th>
                    <th colspan="2" class="border border-black px-2 py-1">Kode</th>
                    <th colspan="2" class="border border-black px-2 py-1">Tanggal</th>
                    <th class="border border-black px-2 py-1">Total Harga</th>
                    <th class="border border-black px-2 py-1">Total Laba</th>
                </tr>
                <tr>
                    <td class="border border-black px-2 py-1">{{ $key+1 }}</td>
                    <td colspan="2" class="border border-black px-2 py-1">{{ $datum->code }}</td>
                    <td colspan="2" class="border border-black px-2 py-1">{{ $datum->created_at->format('d-m-Y H:i') }} {{ config('kasir.timezone') }}</td>
                    <td class="border border-black px-2 py-1 text-right">Rp. {{ number_format($datum->total_price) }},-</td>
                    <td class="border border-black px-2 py-1 text-right">Rp. {{ number_format($datum->getTotalProfit()) }},-</td>
                </tr>

                <tr><td colspan="7" class="py-1"></td></tr>

                <tr class="text-left">
                    <th class="border border-black px-2 py-1">#</th>
                    <th class="border border-black px-2 py-1">Produk</th>
                    <th class="border border-black px-2 py-1">Qty</th>
                    <th class="border border-black px-2 py-1">Harga Beli</th>
                    <th class="border border-black px-2 py-1">Harga Jual</th>
                    <th class="border border-black px-2 py-1">Sub Total</th>
                    <th class="border border-black px-2 py-1">Laba</th>
                </tr>
                @foreach ($datum->items as $keyItem => $item)
                    <tr>
                        <td class="border border-black px-2 py-1">{{ $keyItem+1 }}</td>
                        <td class="border border-black px-2 py-1">{{ $item->product->name ?? '-' }}</td>
                        <td class="border border-black px-2 py-1">{{ $item->qty }}</td>
                        <td class="border border-black px-2 py-1 text-right">Rp. {{ number_format($item->product->price ?? '-') }},-</td>
                        <td class="border border-black px-2 py-1 text-right">Rp. {{ number_format($item->product->getAverageBuyPrice() ?? '-') }},-</td>
                        <td class="border border-black px-2 py-1 text-right">Rp. {{ number_format($item->total_price ?? '-') }},-</td>
                        <td class="border border-black px-2 py-1 text-right">Rp. {{ number_format($item->getProfit()) }},-</td>
                    </tr>
                @endforeach

                <tr><td colspan="7" class="py-4"></td></tr>

            @endforeach

            <tr>
                <td colspan="5" class="border border-black px-2 py-2 text-center font-bold">Total Transaksi</td>
                <td class="border border-black px-2 py-3 text-right">Rp. {{ number_format($totalIncome) }},-</td>
                <td class="border border-black px-2 py-3 text-right">Rp. {{ number_format($totalProfit) }},-</td>
            </tr>
        </table>

    </div>
    <button class="py-3 px-4 bg-blue-400 hover:bg-blue-300 text-white uppercase font-bold rounded-lg shadow-2xl fixed bottom-0 right-0 mb-10 mr-10" onclick="printPage();">Print</button>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script>
        jQuery(document).ready(function($){
            nsZoomZoom();
            $( window ).resize(function() {
                console.log($(window).width());
                nsZoomZoom();
            });

            function nsZoomZoom() {
                htmlWidth = $(window).width();
                pageWidth = $('.page').width();

                if ((htmlWidth * 0.8) > pageWidth)
                    scale = 1
                else {
                    scale = (htmlWidth / pageWidth) * 0.8;
                }

                // Req for IE9
                $(".page").css('transform-origin', 'top left');
                $(".page").css('-ms-transform', 'scale(' + scale + ')');
                $(".page").css('transform', 'scale(' + scale + ')');
            }
        });

        function printPage() {
            $( window ).resize(function(e) {
                return false;
            });

            $(".page").css('transform-origin', 'top left');
            $(".page").css('-ms-transform', 'scale(1)');
            $(".page").css('transform', 'scale(1)');

            setTimeout(window.print(), 1000)
        }
    </script>
</body>
</html>