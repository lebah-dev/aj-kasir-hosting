@extends('layouts.app')
@section('title', 'Product Category Index')

@push('styles')
    <link href="{{ asset('sb-admin-2/vendor/datatables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('contents')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Kategori Produk</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col">
            <div class="card shadow mb-4">
                <div class="card-body">
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('sb-admin-2/vendor/datatables/datatables.min.js') }}"></script>

    {{ $dataTable->scripts() }}
@endpush