@extends('layouts.app')
@section('title', 'Edit Kategori Produk')

@section('contents')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Kategori Produk</h1>
    </div>
    
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="" method="post"> @csrf
                        <div class="form-group row">
                            <label for="nameInput" class="col-12 col-md-4 col-lg-2 col-form-label">Nama Kategori Produk</label>
                            <div class="col-12 col-md-8 col-lg-6">
                                <input type="text" name="name" id="nameInput" class="form-control" value="{{ $data->name }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12 col-md-4 col-lg-2 col-form-label"></div>
                            <div class="col-12 col-md-8 col-lg-6">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <a href="" class="btn btn-light ml-2">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection