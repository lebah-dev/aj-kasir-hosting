<div class="text-nowrap">
    @if (isset($show)) <a href="{{ $show }}" class="text-info border rounded border-info px-2"><i class="fa fa-eye"></i></a> @endif
    @if (isset($print)) <a href="{{ $print }}" target="_blank" class="text-success border rounded border-success px-2"><i class="fa fa-print"></i></a> @endif
    @if (isset($edit)) <a href="{{ $edit }}" class="text-warning border rounded border-warning px-2"><i class="fa fa-edit"></i></a> @endif
    @if (isset($delete)) 
    <a role="button" class="text-danger border rounded border-danger px-2" onclick="$('#delete-{{ $id }}').submit();"><i class="fa fa-trash"></i></a>
    <form id="delete-{{ $id }}" action="{{ $delete }}" method="post" hidden>@csrf @method('delete')</form>
    @endif
</div>