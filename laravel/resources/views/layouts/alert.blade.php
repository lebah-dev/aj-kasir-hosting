@if (session('alert'))
    <div class="row">
        <div class="col">
            <div class="alert alert-{{ session('alert')['class'] }} alert-dismissible fade show" role="alert">
                {{ session('alert')['message'] }}

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
@endif

@if ($errors->any())
    <div class="row">
        <div class="col">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $error }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endforeach
        </div>
    </div>
@endif