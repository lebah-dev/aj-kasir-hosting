<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-sm">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle">
        <i class="fa fa-bars"></i>
    </button>

    <h5 class="mb-0 d-none d-md-block">{{ \App\Models\Shop::get()->name ?? '' }}</h5>
    @if (Shop::get())
    <img class="d-block d-md-none img-crop circle" width="40" height="40" src="{{ Shop::get()->photo }}" alt="">
    @endif

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <li class="nav-item dropdown no-arrow">
            <a class="nav-link" href="#">
                <img src="{{ auth()->user()->avatar }}" alt="" class="d-none d-md-block img-crop circle mr-3" width="40" height="40">
                <span class="text-gray-600 small">Welcome, {{ auth()->user()->name }}</span>
            </a>
        </li>

        <li class="nav-item dropdown no-arrow">
            <a class="nav-link" href="#" onclick="$('#logoutForm').submit()">
                <i class="fa fa-sign-out-alt"></i>
                <span class="ml-2 text-gray-600 small">Logout</span>
            </a>
            <form id="logoutForm" action="{{ route('logout') }}" method="post" hidden>@csrf</form>
        </li>

    </ul>

</nav>