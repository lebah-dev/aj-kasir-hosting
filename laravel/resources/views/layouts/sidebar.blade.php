<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
            <img src="{{ \App\Models\Shop::get()->photo ?? '' }}" class="img-crop circle" width="50" height="50" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">{{ config('app.name') }}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'dashboard') > -1) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'cashier') > -1) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('cashier.index') }}">
            <i class="fas fa-fw fa-calculator"></i>
            <span>Kasir</span>
        </a>
    </li>

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'product_categories') > -1) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('product_categories.index') }}">
            <i class="fas fa-boxes"></i>
            <span>Kelola Kategori Produk</span>
        </a>
    </li>

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'products') > -1) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('products.index') }}">
            <i class="fas fa-box-open"></i>
            <span>Kelola Produk</span>
        </a>
    </li>

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'expenses') > -1) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('expenses.index') }}">
            <i class="fas fa-money-bill"></i>
            <span>Kelola Pengeluaran</span>
        </a>
    </li>

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'transactions') > -1) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#transactionMenu" aria-expanded="false" aria-controls="transactionMenu">
            <i class="fas fa-fw fa-shopping-cart"></i>
            <span>Kelola Penjualan</span>
        </a>
        <div id="transactionMenu" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('transactions.most-sales-products') }}">Barang Terlaku</a>
                <a class="collapse-item" href="{{ route('transactions.manage.index') }}">Transaksi Penjualan</a>
                <a class="collapse-item" href="{{ route('transactions.profit-and-loss') }}">Rugi Laba</a>
            </div>
        </div>
    </li>

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'reports') > -1) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('reports.index') }}">
            <i class="fas fa-fw fa-book"></i>
            <span>Laporan</span>
        </a>
    </li>

    <li class="nav-item {{ (strpos(Route::currentRouteName(), 'settings') > -1) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#settingsMenu" aria-expanded="false" aria-controls="settingsMenu">
            <i class="fas fa-fw fa-cog"></i>
            <span>Pengaturan</span>
        </a>
        <div id="settingsMenu" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('settings.account') }}">Akun</a>
                <a class="collapse-item" href="{{ route('settings.shop') }}">Toko</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>