<!doctype html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transaction {{ $data->code }}</title>

    <link href="{{ asset('css/tailwind.min.css') }}" rel="stylesheet">
</head>
<body class="font-mono text-xs py-2">
    <div class="mx-auto align-content-center" style="width: 58mm;">
        <div class="text-center border-t border-b border-black border-dashed py-2">
            <p class="text-base font-bold">{{ Shop::get()->name ?? '' }}</p>
            <p>{{ Shop::get()->address ?? '' }}</p>
            <p>{{ Shop::get()->phone_number ?? '' }}</p>
        </div>
        <div class="mt-1 border-t border-b border-black border-dashed py-2">
            <table class="w-full">
                <tr><td class="w-1/3">Tanggal</td><td>:</td><td>{{ $data->created_at->format('d-m-Y') }}</td></tr>
                <tr><td>Waktu</td><td>:</td><td>{{ $data->created_at->format('H:m') }} {{ config('kasir.timezone', 'WIB') }}</td></tr>
                <tr><td>Nomor</td><td>:</td><td>{{ $data->code }}</td></tr>
            </table>
        </div>
        <div class="mt-1 border-t border-b border-black border-dashed py-2">
            @foreach($data->items as $item)
                <div class="mb-1">
                    <p>{{ $item->product->name }}</p>
                    <p>
                        <span class="float-left">{{ $item->qty }} x @\{{ number_format($item->product->price) }}</span>
                        <span class="float-right">{{ number_format($item->total_price) }}</span>
                        <span class="clearfix"></span>
                    </p>
                </div>
            @endforeach
        </div>
        <div class="mt-1 border-t border-b border-black border-dashed py-2">
            <table class="w-full">
                <tr><td class="w-1/3">Total</td><td>:</td><td class="text-right">{{ number_format($data->total_price) }}</td></tr>
                <tr><td class="w-1/3">Bayar</td><td>:</td><td class="text-right">{{ number_format($data->cash) }}</td></tr>
                <tr><td class="w-1/3">Kembali</td><td>:</td><td class="text-right">{{ number_format($data->change) }}</td></tr>
            </table>
        </div>
        <div class="mt-1 border-t border-b border-black border-dashed py-2">
            <p class="text-center">Terima Kasih atas Kunjungannya</p>
        </div>
    </div>

    <script>
        // window.print();
    </script>
    <script src="https://cdn.jsdelivr.net/npm/recta/dist/recta.js"></script>
    <script>
        function print() {
            console.log('printing ...');
            var printer = new Recta('1234567890', '1811')
            printer.open().then(function () {
                printer.align('center')
                    .text('Hello World !!')
                    .bold(true)
                    .text('This is bold text')
                    .bold(false)
                    .underline(true)
                    .text('This is underline text')
                    .underline(false)
                    .barcode('CODE39', '123456789')
                    .cut()
                    .print()
            })
        }

        print();
    </script>
</body>
</html>