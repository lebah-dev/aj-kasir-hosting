@extends('layouts.app')
@section('title', 'Kasir')

@push('styles')
    @livewireStyles
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <style>
        .modal {
            display: block;
            overflow-y: auto;
        }

        @media print {
            body * {
                visibility: hidden;
            }
            #print, #print * {
                visibility: visible;
            }
            #print {
                width: 58mm;
                margin-right: auto;
                margin-left: auto;
                margin-top: 0px;
                padding-top: 0px;
            }
        }
    </style>

    <script src="{{ asset('js/alpine.js') }}"></script>
    <script src="{{ asset('js/alpine-ie11.js') }}"></script>
@endpush

@section('contents')
    @livewire('cashier')
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/recta/dist/recta.js"></script>
    @livewireScripts
    <script>
        function print() {
            console.log('printing ...');
            var printer = new Recta('APPKEY', '1811')
            printer.open().then(function () {
                printer.align('center')
                    .text('Hello World !!')
                    .bold(true)
                    .text('This is bold text')
                    .bold(false)
                    .underline(true)
                    .text('This is underline text')
                    .underline(false)
                    .barcode('CODE39', '123456789')
                    .cut()
                    .print()
            })
        }
    </script>
@endpush
