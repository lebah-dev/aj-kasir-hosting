<?php

use App\Models\Expense;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DummyDataSeeder extends Seeder
{
    private $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $user = new \App\Models\User([
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin'),
            ]);
            $user->save();

            $this->createProductCategories(5);
            $this->createProducts(10);
            $this->fillProductStock(1000);
            $this->addTransaction(100);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            dd($e);
        }
    }

    private function createProductCategories(int $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $productCategory = new ProductCategory([
                'name' => $this->faker->word
            ]);
            $productCategory->save();
            $this->command->info('Product Category ' . $productCategory->id . ' created');
        }
    }

    /**
     * @param $count
     */
    private function createProducts(int $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $product = new Product();
            $product->category_id = ProductCategory::query()->inRandomOrder()->first()->id ?? null;
            $product->name = $this->faker->text(rand(15, 35));
            $product->price = rand(10, 30) * 100000;
            $product->size = 1;
            $product->weight = 1;
            $product->description = $this->faker->text;
            $product->image = 'products/images/dummy-product ('.rand(1, 4).').jpg';
            $product->save();

            $this->command->info('Product ' . $product->id . ' created');
        }
    }

    /**
     * @param int $count
     */
    private function fillProductStock(int $count)
    {
        foreach (Product::all() as $product) {
            $expense = new Expense([
                'product_id' => $product->id,
                'price' => $product->price - (rand(10, 30) * 1000),
                'qty' => $count
            ]);
            $expense->total_price = $expense->qty * $expense->price;
            $expense->save();

            $this->command->info('Expense ' . $expense->id . ' created');

            $product->stock += $expense->qty;
            $product->save();
        }
    }

    /**
     * @param int $count
     */
    private function addTransaction(int $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $date = \Carbon\Carbon::now()->subDays(rand(0, 60))->format('Y-m-d H:i:s');

            $transaction = new Transaction();
            $transaction->code = $transaction->getNewCode($date) . '-' . sprintf('%04d', $i);
            $transaction->total_price = 0;
            $transaction->cash = 0;
            $transaction->change = 0;
            $transaction->created_at = $date;
            $transaction->save();

            for ($j = 0; $j < rand(1, 10); $j++) {
                $qty = rand(1, 5);

                $product = Product::query()->inRandomOrder()->first();
                if ($product == null) break;
                if ($product->stock <= 0) break;
                if (($product->stock - $qty) < 0) break;

                $transactionItem = new TransactionItem();
                $transactionItem->product_id = $product->id;
                $transactionItem->qty = $qty;
                $transactionItem->total_price = $product->price * $transactionItem->qty;
                $transactionItem->transaction_id = $transaction->id;
                $transactionItem->created_at = $date;
                $transactionItem->save();

                $product->stock -= $transactionItem->qty;
                $product->save();

                $transaction->total_price += $transactionItem->total_price;
            }

            $transaction->cash = $transaction->total_price + ((rand(0, 50) * 1000));
            $transaction->change = $transaction->total_price - $transaction->cash;
            $transaction->save();

            $this->command->info('Transaction ' . $transaction->id . ' created');
        }
    }
}
