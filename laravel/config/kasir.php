<?php

return [
    /**
     * Indonesian timezone : WIB, WITA, WIT
     */
    'timezone' => env('KASIR_TIMEZONE', 'WIB')
];