$(document).ready(() => {
    $('input:file[data-preview]').on('change', (e) => {
        let imgPreviewElement = $($(e.target).data('preview'));
       imgPreviewElement.attr('src', URL.createObjectURL(e.target.files[0]));
       imgPreviewElement.on('load', () => {
           URL.revokeObjectURL(imgPreviewElement.attr('src'));
       });
    });
});